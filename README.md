# 1- Démarrage de l'application
__Constat__: Les tests ne passent pas, nous avons une exception _java: package org.junit does not exist_

- On ajoute la dépendance JUnit dans le pom.xml

```
<dependencies>
    <!-- Ici j'ajoute les dépendances -->
    
    <!-- Ma dépendance JUnit pour que mes tests compiles -->
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.5</version>
    </dependency>
</dependencies>
```

On relance le programme, cool ca démarre !

L'IA me file un pion, on me demande de la placer avec des coordonnées
Très bien, je tente un 'a', on me dit 'Veuillez entrer des entiers', cool, ce cas est couvert, je mets 5 et je prends un
__java.lang.ArrayIndexOutOfBoundsException__ ce qui signifie que j'ai été en dehors du tableau et l'application plante.

En cherchant rapidement, je m'apercois que dans Grid.java, J'ai une taille de 4 défini en dur

Ma grille fait donc 4 par 4 et si je pose un pion en dehors, j'ai un plantage, le comportement n'est pas top.

Ce que je veux, c'est que si je pose un pion en dehors, je lève une exception et je propose au joueur de rejouer après l'avoir notifié.

## 1er Test
Je vérifie que si je mets une coordonnée en dehors de la taille de la grille, je lève une exception

Comme on teste la grille, on va créer un fichier __GridUnitTest__

Le nom de mon test doit être clair

Je crée une méthode 

```java
@Test
public void whenPionIsputtedOutsideTheGridItShouldThrowException(){
    
}
```

Pour que mon test soit valide, je vais devoir initié une grille que je vais utiliser dans mes tests
Je vais donc créer une méthode que je vais annoter ```@Before``` pour initier mon contexte

Pour comprendre ces annotations => https://www.baeldung.com/junit-before-beforeclass-beforeeach-beforeall

Dans ma méthode annoté @Before, on va initié une grille toute neuve ;)

```java
@Before
public void initTestOnGrid(){
    // J'initialise une grille
    grid = new Grid(new PionBox(PionBox.piecesFactory()));
}
```

Désormais, je vais aoir une grille de 4 par 4 initié dans tous mes tests

Ecriture de mon test :

```java
@Test(expected = ArrayIndexOutOfBoundsException.class)
public void whenPionIsputtedOutsideOfTheGridItShouldThrowException() throws PionMemePlaceException {
    grid.ajoutPion(new Position(5,5), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE));
}
```

Super ! Notre premier test passe !

On continue sur notre lancé. On a une regle qui dit que si on pose un pion sur un emplacement ou il y a déjà un pion, on doit lever une exception 


```java
@Test(expected = PionMemePlaceException.class)
public void whenPionIsputtedOnAPositionWhereAnotherPionIsPresentItShouldThrowException() throws PionMemePlaceException {
    grid.ajoutPion(new Position(0,0), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE));
    grid.ajoutPion(new Position(0,0), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE));
}
```
