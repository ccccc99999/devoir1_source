package quarto;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import quarto.enumeration.Coeur;
import quarto.enumeration.Couleur;
import quarto.enumeration.Forme;
import quarto.enumeration.Taille;
import quarto.exception.PionMemePlaceException;

public class GridUnitTest {

    private Grid grid;

    @Before
    public void initTestOnGrid(){
        // J'initialise une grille
        grid = new Grid(new PionBox(PionBox.piecesFactory()));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void whenPionIsputtedOutsideOfTheGridItShouldThrowException() throws PionMemePlaceException {
        grid.ajoutPion(new Position(5,5), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE));
    }

    @Test(expected = PionMemePlaceException.class)
    public void whenPionIsputtedOnAPositionWhereAnotherPionIsPresentItShouldThrowException() throws PionMemePlaceException {
        grid.ajoutPion(new Position(0,0), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE));
        grid.ajoutPion(new Position(0,0), new Piece(Couleur.FONCEE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE));
    }

    @Test
    public void whenIPutAPionThisOneShouldNotBeAvailableToPla() throws PionMemePlaceException {
        Piece pionAJouer = new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE);
        grid.ajoutPion(new Position(0,0), pionAJouer);
        Assert.assertTrue(!grid.getPionsDisponibles().contains(pionAJouer));
    }

    @Test
    public void whenIPutAPionMyHistoricShouldBeIncrementedByOne() throws PionMemePlaceException {
        int historicSizeBeforeIPlay = grid.getSizeOfHistory();
        Piece pionAJouer = new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE);
        grid.ajoutPion(new Position(0,0), pionAJouer);
        int historicSizeAfterIPlay = grid.getSizeOfHistory();
        Assert.assertTrue(historicSizeAfterIPlay == historicSizeBeforeIPlay + 1);
    }

    // Test de la méthode estGagné
    // Predicact :
    // Si j'ai déjà 3 pions alignés de la même couleur, si j'ajoute un 4ème pion, la méthode estGagné
    // devrait me retourner true
    //Il n'y a pas de caractéristique du pion posé ?
    @Test
    public void quatreAlignesVertical(){
        grid.ajoutPion(new Position(0,4), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE));
        grid.ajoutPion(new Position(1,4), new Piece(Couleur.CLAIRE, Forme.RONDE, Taille.BASSE, Coeur.CREUSE));
        grid.ajoutPion(new Position(2,4), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.HAUTE, Coeur.CREUSE));
        Assert.assertTrue(estGagnee(3,4));
            }

     // Si j'ai déjà 3 pions alignés de la même couleur, il existe une position permettant de gagner
    // existWinningPosition devrait me retourner true
    @Test
    public void existWinningVertical(){
        grid.ajoutPion(new Position(0,4), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE));
        grid.ajoutPion(new Position(1,4), new Piece(Couleur.CLAIRE, Forme.RONDE, Taille.BASSE, Coeur.CREUSE));
        grid.ajoutPion(new Position(2,4), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.HAUTE, Coeur.CREUSE));
        Piece pionAJouer = new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE);
        Assert.assertTrue(existWinningPosition(PionAJouer));
            }

    // test du UNDO
    // positionné un pion, puis faire UNDO, la case doit etre libre
    @Test
     public void annulerMouvement(){
        grid.ajoutPion(new Position(0,0), new Piece(Couleur.CLAIRE, Forme.CARREE, Taille.BASSE, Coeur.CREUSE));
        grid.undo();
        Assert.assertNULL(0,0));
            }

}
