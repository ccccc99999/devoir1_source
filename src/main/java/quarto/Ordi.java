package quarto;

import quarto.exception.PionMemePlaceException;
import quarto.exception.PlayImpossibleException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ordi implements Player {

    public String name;
    protected Grid grille;

    public Ordi(String name, Grid grille) {
        this.name = name;
        this.grille = grille;
    }

    public Piece getPieceToPlay() {
        try{
            return getUnriskedPieceToPlay();
        }
        catch (PlayImpossibleException e) {
            Random random = new Random();
            List<Piece> pionsDisponibles = grille.getPionsDisponibles();
            Piece piece = pionsDisponibles.get(random.nextInt(pionsDisponibles.size()));
            grille.rendIndisponible(piece);
            return piece;
        }
    }

    public Piece getUnriskedPieceToPlay() throws PlayImpossibleException {
        List<Piece> pionsDisponibles = grille.getPionsDisponibles();
        pionsDisponibles.removeAll(filtrePionsGagnants(pionsDisponibles));
        if (pionsDisponibles.isEmpty()) throw new PlayImpossibleException();
        Random random = new Random();
        Piece piece = pionsDisponibles.get(random.nextInt(pionsDisponibles.size()));
        grille.rendIndisponible(piece);
        return piece;
    }

    private List<Piece> filtrePionsGagnants(List<Piece> pionsDisponibles) {
        List<Piece> pionsGagnants = new ArrayList<Piece>();
        for (Piece pion : pionsDisponibles) {
            if (grille.existWinningPosition(pion))
                pionsGagnants.add(pion);
        }
        return pionsGagnants;
    }

    public Position getPositionToPlay(Piece p) {
        List<Position> positionsJouables = grille.collectFreePositions();
        boolean foundPosition = false;
        for (Position positionJouable : positionsJouables) {
            try {
                grille.ajoutPion(positionJouable, p);
            } catch (PionMemePlaceException e) {
                throw new RuntimeException("it can't be");
            }
            if (grille.estGagnee(positionJouable)) {
                foundPosition = true;
            }
            grille.undo();
            if (foundPosition) return positionJouable;
        }
        Random random = new Random();
        return positionsJouables.get(random.nextInt(positionsJouables.size()));
    }

    public boolean declareQuarto(Position pos) {
        return grille.estGagnee(pos);
    }

    public void win() {
        System.out.println("QUARTO");
    }
}
