package quarto;

import quarto.exception.PionMemePlaceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Grid {
	private Piece[][] plateau;
	private Stack<Position> hist;
	public static final int TAILLE = 4;
    private PionBox box;

    public Grid(PionBox box) {
        this.box = box;
        plateau = new Piece[TAILLE][TAILLE];
		hist = new Stack<Position>();
	}

	public List<Piece> getPionsDisponibles() {
		return box.getPionsDisponibles();
	}

    public void ajoutPion(Position pos, Piece pionAJouer) throws PionMemePlaceException {
        int x = pos.getX();
        int y = pos.getY();
        if (plateau[x][y] != null)
            throw new PionMemePlaceException();

        this.plateau[x][y] = pionAJouer;
        Position p = new Position(x, y);
        hist.push(p);
    }

    public int getSizeOfHistory(){
        return this.hist.size();
    }

    public void undo() {
		Position p = hist.pop();
		plateau[p.getX()][p.getY()] = null;
	}

	public boolean estGagnee(Position pos) {
        if (extractColumn(pos.getY()).isWon()) return true;
        if (extractLine(pos.getX()).isWon()) return true;
        if (pos.getX() == pos.getY() && extractFirstDiagonal().isWon()) return true;
        if (pos.getX() + pos.getY() == 3 && extractSecondDiagonal().isWon()) return true;
		return false;
	}

	public boolean isPat() {
		return (box.isEmpty());
	}

    public void rendIndisponible(Piece piece) {
        box.remove(piece);
    }

    public Line extractLine(int index) {
        Line line = new Line();
        for (int i = 0; i < TAILLE; i++) {
            if (plateau[index][i]!=null)
                line.add(plateau[index][i]);
        }
        return line;
    }

    public Line extractColumn(int index) {
        Line line = new Line();
        for (int i = 0; i < TAILLE; i++) {
            if (plateau[i][index]!=null)
                line.add(plateau[i][index]);
        }
        return line;
    }

    public Line extractSecondDiagonal() {
        Line line = new Line();
        for (int i = 0; i < TAILLE; i++) {
            if (plateau[i][TAILLE-1-i]!=null)
                line.add(plateau[i][TAILLE-1-i]);
        }
        return line;
    }

    public Line extractFirstDiagonal() {
        Line line = new Line();
        for (int i = 0; i < TAILLE; i++) {
            if (plateau[i][i]!=null)
                line.add(plateau[i][i]);
        }
        return line;
    }

    public void display() {
        for (int i = 0; i < TAILLE; i++) {
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < TAILLE; j++) {
                if (plateau[i][j] == null) builder.append("                                     | ");
                else builder.append(plateau[i][j]).append(" | ");
            }
            System.out.println(builder.toString());
            System.out.print("------------------------------------------------------------------------------------");
            System.out.println("------------------------------------------------------------------------------------");
        }
    }

    public boolean isFreePosition(Position pos) {
        return (plateau[pos.getX()][pos.getY()]==null);
    }

    boolean existWinningPosition(Piece pion) {
        for (int i = 0; i < 4; i++) {
            Line rangee = extractLine(i);
            rangee.add(pion);
            if (rangee.isWon()) return true;
        }
        for (int i = 0; i < 4; i++) {
            Line rangee = extractColumn(i);
            rangee.add(pion);
            if (rangee.isWon()) return true;
        }
        Line rangee = extractFirstDiagonal();
        rangee.add(pion);
        if (rangee.isWon()) return true;
        rangee = extractSecondDiagonal();
        rangee.add(pion);
        return rangee.isWon();
    }

    List<Position> collectFreePositions() {
        List<Position> positions = new ArrayList<Position>();
        for (int i = 0; i < TAILLE; ++i)
            for (int j = 0; j < TAILLE; ++j) {
                Position pos = new Position(i, j);
                if (isFreePosition(pos)) {
                    positions.add(pos);
                }
            }
        return positions;
    }
}
