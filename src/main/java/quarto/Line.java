package quarto;

import quarto.enumeration.Coeur;
import quarto.enumeration.Couleur;
import quarto.enumeration.Forme;
import quarto.enumeration.Taille;

import java.util.ArrayList;
import java.util.List;

public class Line {

    private List<Piece> pieces;

    public Line() {
        pieces = new ArrayList<Piece>();
    }

    public void add(Piece piece) {
        pieces.add(piece);
    }

    public boolean isWon() {
        if (pieces.size() < 4) return false;
        boolean fonce = true;
        boolean clair = true;
        boolean rond = true;
        boolean carre = true;
        boolean plein = true;
        boolean creux = true;
        boolean court = true;
        boolean longue = true;
        for (Piece piece : pieces) {
            fonce = fonce && (piece.couleur == Couleur.FONCEE);
            clair = clair && (piece.couleur == Couleur.CLAIRE);
            rond = rond && (piece.forme == Forme.RONDE);
            carre = carre && (piece.forme  == Forme.CARREE);
            plein = plein && (piece.coeur == Coeur.PLEINE);
            creux = creux && (piece.coeur == Coeur.CREUSE);
            court = court && (piece.taille == Taille.BASSE);
            longue = longue && (piece.taille == Taille.HAUTE);
        }
        return fonce || clair || rond || carre || plein || creux|| court || longue;

    }

}
